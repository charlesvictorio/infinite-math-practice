GOAL: Write down the important features.
NOTES:
- For eq sys, polyn arith, funcs, etc: transform eqs to make truly infinite.
TODO:
- Functions > Linear: Find which generates happier funcs: `lc.py` or `lc2.py`.

gelada research paper
back up computer (upload config files to github).
- i3, vim, spacemacs, bash[rc|aliases|functions],
infmath
ISLR

./infinite_math:
polynomial.py
quadratic.py
statistics.py

./wip_topics:
exponent.py
lc2.py
lc.py
rad_drills.py
storage.py

# Function Graphing
lin, quad, cubic, abs, expt
eq <-> graph

Generate happy absolute value!!!!!
1. Pick x_int
2. Pick vertex (can't have same x or y)
3. Scale and shift to be truly infinite
Shift: a|x - (h + s_n)| + (k + s_k)
Scale: vert -> (x_v * s_v, y_v * s_v), xint -> (x_{int} * s_v, 0)

# Equation Systems
1-2 equations or inequalities.
types: lin, quad, cubic, abs, expt

Generate 1-2 in/eqs with random parts.
- Solve 1 expr for zero(s). Ex: `2x + 3`
  No expt,
- Solve 1 inequality for x interval(s). Ex: `x^2 + 3 > 7x - 2`
- Solve 2 equalities for (x, y).
  Ex: ```{y = -6|x + 3| - 1
        {y = -x^2 - 8```
- Solve 2 inequalities for intersecting area.
  Ex: ```{y < 2x^2 - 3x + 1
         {y > -x - 7```

# Radicals, Rational Exponents
bd: https://www.khanacademy.org/math/algebra2/x2ec2f6f830c9fb89:exp
Convert between radical form and exponent form.
Simplify radicals. Ex: sqrt(12), root(3, 27x^7y^3)
Simplify radical + rational + rat expts expressions with operations (+,-,\*,/) between them.

# Exponents
Solve exponential equations (ex: 2^x = 4 = 2^2, x = 2) (use equality of bases).
Bases: 2, 3, 5
Logarithms?
More to add in EM3

# 'Anything Goes' Eqs
Parts: decimals, fractions, exponents, radicals, complex numbers, abs vals?
Generate parts in the holes of parts.

# Polynomial Extra Drills
Linear:
points -> eq
Quadratic:
points -> eq
Convert expressions between forms (standard, vertex, factored).
Solve for x using inverse operations, zero product property, completing the square.
Cubic:
todo
# Polynomial Arithmetic
Simplify expressions (+, -, \*, /, factorization).
# Later
https://www.khanacademy.org/math/algebra2
Rational exponents and radicals
Logarithms
Transformations of functions
Rational functions
Trigonometry
## Statistics
Normal distribution / z-scores
Sample statistics: prop, mean, diff betw 2 props, diff betw 2 means
Sampling distributions of sample statistics
Confidence intervals from sample statistics
Chi-squared (Goodness of fit, Independence)
ANOVA

### Probability
Use P(A), P(B), P(A \cap B), P(A \cup B), P(A | B) to find others.
frequency table -> joint/marginal/conditional probabilities
Combinatorics (permu/combos, fcp, permus w/ repetition, etc)
Discrete probability distributions (binomial, hypergeometric, etc)
