GOAL: Write down the important features.
NOTES:
- For eq sys, polyn arith, funcs, etc: transform eqs to make truly infinite.

# Equation Systems
1-2 equations or inequalities.
Parts
- exponents
    - natural numbers. Makes polynomial terms. Ex: 2x^3
    - variables. makes exponential terms. Ex: 5^2x
    - other parts. Ex: 3^(x^2 - 4)
- fractions. parts in num and den
- all number systems: decimals, radicals, complex numbers
- absolute values
All of these are added and multiplied together. Subtracted by adding negative terms. Divided by fractions, multiplying decimals, negative exponents.
This will not have: step, piecewise,

Generate 1-2 in/eqs with random parts.
- Solve 1 equality for x value(s). Ex: `2x + 3 = 2|x - 5| + 6`
- Solve 1 inequality for x interval(s). Ex: `x^2 + 3 > 7x - 2`
- Solve 2 equalities for (x, y).
  Ex: ```{y = -6|x + 3| - 1
        {y = -x^2 - 8```
- Solve 2 inequalities for intersecting area.
  Ex: ```{y < 2x^2 - 3x + 1
         {y > -x - 7```

# Functions
Function families: lin, quad (3 forms), cubic (3 forms), abs, expt, step (round up/down), piecewise
For some families like quadratic, there are other drills specific to that family.
Drills
- eq -> graph
- graph -> eq
- eq, x -> f(x)
- graph, x -> f(x)
- points -> eq
- eq -> average rate of change over an interval (2 points -> avg roc)
- graph -> average rate of change over an interval (2 points -> avg roc)
- graph -> key features
    - domain, range, end behavior
    - intercepts
    - inc/dec/pos/neg intervals
    - rel/abs min/max extrema

# Polynomial Arithmetic
Simplify expressions (+, -, \*, /, factorization).

# Radicals, Rational Exponents
Convert between radical form and exponent form.
Simplify radicals. Ex: sqrt(12), root(3, 27x^7y^3)
Simplify radical + rational + rat expts expressions with operations (+,-,\*,/) between them.
add more

# Exponents
Solve exponential equations (ex: 2^x = 4 = 2^2, x = 2) (use equality of bases).
Bases: 2, 3, 5
Logarithms?
More to add in EM3

# Quadratic
Convert expressions between forms (standard, vertex, factored).
Solve for x using inverse operations, zero product property, completing the square.

***

# Trigonometry

# Statistics
## Probability
Use P(A), P(B), P(A \cap B), P(A \cup B), P(A | B) to find others.
frequency table -> joint/marginal/conditional probabilities
Combinatorics (permu/combos, fcp, permus w/ repetition, etc)
Discrete probability distributions (binomial, hypergeometric, etc)
