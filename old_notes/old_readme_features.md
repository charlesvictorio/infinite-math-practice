## Features
### Function Representation Conversions:
Families: Linear, Quadratic, Absolute Value, Exponential, Piecewise
All
- Graph (eq -> graph)
- Find the equation (graph -> eq)
- Find the domain, range, and end behavior (graph -> d, r, eb)
- Identify the function family (eq -> family)  # Example item: sorting mult funcs into categories
- Identify the function family (graph -> family)
- Find function's output when given an input (f/eq, x -> f(x))
- No piecewise?: Identify the function family (table of (x, y) -> family)
- No piecewise: Describe transformations from parent function

Linear
- Find the equation (2 points -> eq)
- Find the equation (1 point, slope)
- Find the equation (1 point, parallel or perpendicular to a linear function)

Quadratic And Absolute Value
- Find the equation (3 points -> eq)
- Find the equation (2 points, vertex -> eq)
- Find the intercepts (graph -> intercepts)
- Find the inc/dec/pos/neg intervals (graph -> intervals)
- Find the rel/abs min/max (graph -> extrema)
- Find the average rate of change over an interval (points -> slope)


### Quadratic
Convert quadratic equations between standard, vertex, factored forms (involves irrational and complex numbers).
Find solutions to quadratic expressions. Use inverse operations, zero product property, completing the square, quadratic formula.

### Polynomials
Apply operations (+,-,\*) on polynomials.
PST and DOTS
After Learning: Divide and factor polynomials.

### Equations + Systems
Solve linear/quadratic/abs in/equalities:
- Solve eq1 = eq2 for x from their equation (expressions). eq2 can be constant
- Solve eq systems for (x, y) from their equations and graphs.

### Exponents
Solve exponential equations (ex: 2^x = 4 = 2^2, x = 2) (use equality of bases).
Bases: 2, 3, 5
eq -> graph
graph -> eq
More to add in EM3

### Radicals, Rational Exponents
Convert between radical form and exponent form.
Simplify numerical radicals
Simplify radical + rational + rat expts expressions with operations (+,-,\*,/) between them.

### Complex Numbers
Find powers of i.
z eq -> conj, abs val
z eq <-> complex plane

### Step Functions
graph <-> eq

### Expressions
Simplify and apply operations on expressions. Many variables, all number systems (fracs, rads, comp), abs val, step?.


### Statistics

#### Probability
Use P(A), P(B), P(A \cap B), P(A \cup B), P(A | B) to find others.
frequency table -> joint/marginal/conditional probabilities
Combinatorics (fundamental counting principle, permus + w/ repetition, combos, etc)
Discrete probability distributions (binomial, hypergeometric, etc)