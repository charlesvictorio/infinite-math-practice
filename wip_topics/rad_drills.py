"""Practice Radicals And Rational Exponents

Drills:
Convert terms between radical and exponent form.
Simplify terms, and apply operations terms
Simplify expressions with radicals and rational exponents.
"""

from math import gcd
from random import random, randint, choice
from sympy import sqrt, latex

class Radicals:
    @staticmethod
    def rad2expt(direction='r2e') -> (str, str):
        """Practice converting simple radicals between radical and exponent form.

        Inputs:
            direction (str): If 'r2e', output is (radical form, exponent form).
                            If 'e2r' (or anything else), output is reversed.
        Outputs: Tuple of Tex representation of radicals in both forms.
        """

        # Make sure the fractions are in simplest form so all are equally likely.
        expt_options = [[num for num in range(1, 11) if gcd(num, den) == 1] for den in range(2, 6)]
        index = randint(2, 5)
        exponent = choice(expt_options[index - 2])
        str_root = '\sqrt' if index == 2 else f'\sqrt[{index}]'
        str_expt = '' if exponent == 1 else f'^{{{exponent}}}'

        # sqrt(x^3) or sqrt(x)^3
        if random() < 0.5:
            # Ex:  3 _____
            #      \/ x^2
            rad_form = str_root + '{x' + str_expt + '}'
        else:
            # Ex: / 3 ___ \ ^ 2
            #     \ \/ x  /

            # Don't put parenthesis if exponent is 1.
            oparen, cparen = ('', '') if exponent == 1 else '()'
            rad_form = oparen + str_root + '{x}' + cparen + str_expt
        expt_form = f'x^\\frac{{{exponent}}}{{{index}}}'

        return (rad_form, expt_form) if direction == 'r2e' else (expt_form, rad_form)

    @staticmethod
    def simp_sqrt() -> (str, str):
        """Practice simplify square roots.

        Generate happy radicals:
        I want the simplified root to be in (2, 3, 5, 6, 7).
        I want the coefficient to be in [1-10].
        So, to get a happy radicand: Pick a root, pick a coeff, multiply them together.
        The possible radicands have been precomputed.
        """

        # [root * perf_sq for root in (2, 3, 5, 6, 7) for perf_sq in [4, 9, 16, 25, 36, 49, 64, 81, 100]]
        happy_radicands = [
            8, 12, 18, 20, 24, 27, 28, 32, 45, 48, 50, 54, 63, 72, 75, 80, 96, 98,
            108, 112, 125, 128, 147, 150, 162, 175, 180, 192, 200, 216, 243, 245,
            252, 294, 300, 320, 343, 384, 405, 448, 486, 500, 567, 600, 700
            ]

        radicand = choice(happy_radicands)
        return (str(radicand), latex(sqrt(radicand)))
