"""Linear Function Drills

TODO:
Make a general 'find the equation' drill function using all 3 funcs.
Make graph edges auto-adjust to fit function key features.

Drills:
eq -> graph
graph -> eq
eq -> zeros
points -> eq
"""

from fractions import Fraction
from uuid import uuid4
import numpy as np
from numpy.random import choice, randint
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.style.use('fivethirtyeight')

class Linear:
    def __init__(self, edge=5):
        """Make a linear function so all of its important points are inside the edge.

        Important points:
        - y-intercept
        - Another point on the line. The 2 points will be used to find the slope using
          $m = (y2 - y1) / (x2 - x1)$.

        The slope should be a rational number.
        The coordinates of the vertex and y intercept should be integers.
        To ensure this:
        Pick the y intercept. Then pick another point (x != 0).
        """

        # All points should be inside the graph.
        # This var is the maximum value for all points.
        # Usually 5 or 10.
        self.edge = edge

        # Pick the coordinates for the y intercept (0, b).
        # x1 = 0 by definition.
        y1 = randint(-edge, edge + 1)
        # Pick the coordinates for point 2 (x2, y2).
        # The x value can't be 0 since this isn't the y intercept.
        x2 = randint(1, edge + 1) * choice([1, -1])
        y2 = randint(-edge, edge + 1)

        # Calculate the slope using $m = (y2 - y1) / (x2 - x1)$.
        # Again, I know that x1 = 0.
        m = Fraction(y2 - y1, x2)

        # Simplify integer slopes: m = 4 / 2 = 2 / 1 = 2
        if m.denominator == 1:
            m = m.numerator

        self.m = m
        self.b = b

    @staticmethod
    def from_str(m, b):
        m = str(m) if not m in (1, -1) else ('' if m == 1 else '-')
        b = '' if b == 0 else (f' + {b}' if b > 0 else f' - {-b}')
        return f'{m}x{b}'
    def __str__(self):
        return Lin.from_str(self.m, self.b)

    def __repr__(self):
        """Return the representation of the linear function.

        Since the function was randomly generated, the representation is inexact."""

        return f'Linear({self.edge})'

    @staticmethod
    def latex_frac(q: Fraction) -> str:
        return f'\\frac{{{q.numerator}}}{{{q.denominator}}}'

    def graph2eq_drill():
        """Find the equations from the graphs."""

        fig, ax = plt.subplots()
        f = Linear()
        if isinstance(f.m, Fraction):
            re_ratio = f.m * Linear.edge + f.b
            right_edge = re_ratio.numerator / re_ratio.denominator
            le_ratio = f.m * -Linear.edge + f.b
            left_edge = le_ratio.numerator / le_ratio.denominator
        else:
            left_edge = f.m * -Linear.edge + f.b
            right_edge = f.m * Linear.edge + f.b
        ax.plot((-Linear.edge, Linear.edge), (left_edge, right_edge))
        ax.set_xlim(-Linear.edge, Linear.edge)
        ax.set_xticks(np.arange(-Linear.edge, Linear.edge + 1, Linear.edge / 5))
        ax.set_ylim(-Linear.edge, Linear.edge)
        ax.set_yticks(np.arange(-Linear.edge, Linear.edge + 1, Linear.edge / 5))
        # ax.set_title(f'${lin_str(f.m, f.b)}$')
        ax.annotate('', xy=(-Linear.edge, 0), xytext=(Linear.edge, 0), arrowprops=dict(arrowstyle='<|-|>', color='k'), size=20,         zorder=1)
        ax.annotate('', xy=(0, -Linear.edge), xytext=(0, Linear.edge), arrowprops=dict(arrowstyle='<|-|>', color='k'), size=20,         zorder=1)
        path = f'../pics/graph_{uuid4()}.png'
        plt.savefig(path)
        return (str(f), path)


    # @staticmethod
    # def find_fx_drill():
    #     """For these functions, find the given value of f(x)."""

    #     edge = 5
    #     f = Linear(edge)
    #     x = randint(-edge, edge + 1)
    #     fx = f.m * x + f.b
    #     # Shouldn't this check if `f.m` is a Fraction?
    #     str_fx = Linear.latex_frac(fx) if isinstance(fx, Fraction) else str(fx)
    #     return (f'f(x) = {str(f)} \\text{{, find }} f({x})', str_fx)

    # @staticmethod
    # def dreb_drill():
    #     """Find the domain, range, and end behavior of these functions."""

    #     # To render this to worksheet, it will be inside $$. Delimit with \text{}?
    #     f = Linear(Linear.edge)
    #     domain = '\\mathbb{R}' # for domain and range, it should be in $$.
    #     # If m = 0, the graph of f(x) is a vertical line.
    #     range_ = '\\mathbb{R}'  if f.m != 0 else str(f.b)
    #     if f.m > 0:
    #         fx_pi = '\\infty'
    #         fx_ni = '- \\infty'
    #     elif f.m < 0:
    #         fx_pi = '- \\infty'
    #         fx_ni = '\\infty'
    #     else:
    #         fx_pi = fx_ni = str(f.b)

    #     # TODO: make eq renderer '\item q' instead of '\item $q$'. Put $ in drill funcs.
    #     end_behavior = f'\\text{{As }} x \\rightarrow \\infty \\text{{, }} f(x) \\rightarrow {fx_pi}.\\\\\\text{{As }} x \\rightarrow - \\infty \\text{{, }} f(x) \\rightarrow {fx_ni}.'
    #     # end_behavior = f'As $x \\rightarrow \\infty$, $f(x) \\rightarrow {fx_pi}$.\\\\As $x \\rightarrow - \\infty$, $f(x) \\rightarrow {fx_ni}$.'
    #     dr_str = f'f: {domain} \\rightarrow {range_}\\\\'
    #     return (f'f(x) = {f}', f'{dr_str}\\text{{End Behavior:}}\\\\ {end_behavior}')

    # @staticmethod
    # def parent_transformations_drill() -> (str, str):
    #     """Describe the transformations from the parent function.

    #     How did y = x go to y = mx + b?
    #     Vertical stretching (a)
    #     Reflecting over the x-axis (if a is negative)
    #     Translating left and right, or up and down (h, k).
    #     """

    #     f = Linear()
    #     if f.m == 1 and f.b == 0:
    #         return (str(f), 'This is the parent function.')
    #     str_m = Linear.latex_frac(abs(f.m)) if isinstance(f.m, Fraction) else abs(f.m)
    #     steps = f'Vertically stretch by a factor of ${str_m}$.' if abs(f.m) != 1 else ''
    #     if f.m < 0:
    #         steps += ' Reflect over the x-axis.'
    #     if f.b != 0:
    #         updown_dir = 'up' if f.b > 0 else 'down'
    #         unit = '' if abs(f.b) == 1 else 's'
    #         steps += f' Tranlate {updown_dir} by {abs(f.b)} unit{unit}.'
    #     return (str(f), '\\text{{' + steps + '}}')
