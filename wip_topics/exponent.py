"""Solve exponential equations using equality of bases.

Ex: 2^x = 4 = 2^2, x = 2
Bases: 2, 3, 5

5^(6x + 6) = 125^x
5^(6x + 6) = 5^3x
6x + 6 = 3x
3x = -6
x = -2
"""

from infinite_math import Polynomial
import numpy as np
from numpy.random import choice, randint
from sympy import *
x = symbols('x')

def eob():
    """Practice solving exponential equations using equality of bases."""
    base = choice([2, 3, 5])
    r_pow = randint(1, 5) #* choice([1, -1])
    l_pow = randint(1, 5) #* choice([1, -1])
    r_pow_eq = Polynomial.gen()
    l_pow_eq = Polynomial.gen()

    r = (base ** r_pow) ** r_pow_eq
    l = (base ** l_pow) ** l_pow_eq
    print(f'{latex(r)} = {latex(l)}')
    print(solveset(Eq(r, l)))

def gen_expt():
    """Generate an exponential equation.
https://www.mathsisfun.com/sets/function-exponential.html
Get exponential function key features from graph:
f(x) = a * b^(x + c) + d

a: If positive, f(x) trends to infinity. If negative, f(x) trends to negative infinity.

b
What it is: If 1, constant term. If >1, f(x) trends to +/- infinity. If <1, f(x) trends to 0.
How to find it: Find (c, d + 1). b is y - d value when x is c + 1

c
What is it: Translates f(x) c units left.
How to find it: c is -1 * the x value when y is 1 unit above/below the horizontal asymtote.

d
What is it: Translates f(x) d units up.
How to find it: d is the horizontal asymtote.

(c, d + 1) is the translated y^0 point. (c + 1, d + b) is the translated y^1 point.
    """
    pass
