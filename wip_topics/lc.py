"""Linear Function Conversions

Drills:
eq -> graph
graph -> eq
eq -> zeros
points -> eq

TODO: Better conversion from fracs to decs."""
from __future__ import annotations
from fractions import Fraction
import numpy as np
from numpy.random import randint, choice

class Lin:
    def __init__(self, m, b):
        """Make or randomly generate a linear function.

        y = mx + b

        Ex:
            Make the linear function `3x + 6`
            >>> y = Lin(3, 6)
            Generate a linear function.
            m is an int within [-3, 3].
            y is an int within [-10, 10].
            >>> y = Lin((-3, 3), (-10, 10))
        """

        if isinstance(m, (int, float)):
            self.m = m
        elif isinstance(m, tuple) and len(m) == 2:
            assert len(m) == 2 and m[0] < m[1] + 1, 'Slope range must be a tuple of 2 ints in increasing order.'

            # For linear equations, m != 0
            if m[0] <= 0 <= m[1] + 1:
                self.m = choice(np.setdiff1d(np.arange(m[0], m[1] + 1), 0))  # Remove 0 from possibilities
            else:
                self.m = randint(m[0], m[1] + 1)
        if isinstance(b, (int, float)):
            self.b = b
        elif isinstance(b, tuple) and len(b) == 2:
            self.b = randint(b[0], b[1] + 1)

    def __call__(self, x):
        return self.m * x + self.b
    def __repr__(self):
        return f'Lin({self.m}, {self.b})'
    @staticmethod
    def from_str(m, b):
        m = str(m) if not m in (1, -1) else ('' if m == 1 else '-')
        b = '' if b == 0 else (f' + {b}' if b > 0 else f' - {-b}')
        return f'{m}x{b}'
    def __str__(self):
        return Lin.from_str(self.m, self.b)
    def __eq__(self, other):
        return True if (self.m == other.m and self.b == other.b) else False

    @staticmethod
    def happy_m(fraction=False):  # Rename to happy_rat?
        """Return a 'simple' rational slope."""

        m = (randint(0, 5) + choice([0, 0.5, 0.25, 0.75, 0.2, 0.4, 0.6, 0.8])) * choice([1, -1])
        if fraction:
            # Convert decimal to fraction, use ld() to prevent floating point errors.
            # The denominator is [2, 5].
            return Fraction(m).limit_denominator(5).as_integer_ratio()
        return m

    @staticmethod
    def points() -> (((float, float), (float, float)), str):
        """Practice finding the equation of a linear function with 2 points on the line.

        First find m. It must be a 'simple' fraction so the points won't be too far apart.
        Then set x1 and y1 to random values.
        Use (y2 - y1) / (x2 - x1) = m to find x2 and y2.
        Plug in (x1, y1) to y = mx + b to find b.
        """

        num, den = Lin.happy_m(fraction=True)
        m = round(num / den, 2)
        x1, y1 = randint(-5, 6, 2)
        x2 = x1 + den
        y2 = y1 + num
        b = round(y1 - (m * x1), 2)  # Prevent floating point errors.
        b = int(b) if int(b) == b else b  # Round b to make cleaner.
        print(f'({y2} - {y1}) / ({x2} - {x1}) = {num} / {den}')  # db
        return (((x1, y1), (x2, y2)), Lin.from_str(m, b))

    # TODO: Turn this into a drill.
def plot_lin(expr, eq_title=False):
    """Plot a linear function."""

    f = lambdify(expr.free_symbols.pop(), expr, 'numpy')
    # TODO: adjust visual domain (xlim) to center around key features (in this case y-int)
    x = np.arange(-10, 10, 0.1)
    y = f(x)

    fig, ax = plt.subplots()
    ax.plot(x, y)
    if eq_title:
        # Render equation as title. Makes 'x^2' into actual superscript.
        ax.set_title(rf'${expr}$')

    # Boundaries
    edge = 10
    ax.set_xlim(-edge, edge)
    ax.set_xticks(np.arange(-edge, edge + 1, edge / 5))
    ax.set_ylim(-edge, edge)
    ax.set_yticks(np.arange(-edge, edge + 1, edge / 5))

    # Axis lines
    ax.axhline(0, c='k', alpha=0.5)
    ax.axvline(0, c='k', alpha=0.5)
    plt.show()


    # @staticmethod
    # def parent_transfs() -> (str, str):
    #     """Practice using words to describe linear functions as transformations of the parent function.

    #     y = x -> y = mx + b
    #     """

    #     f = Lin((-5, 5), (-5, 5))
    #     if f == Lin(1, 0):
    #         return (str(f), 'This is the parent function.')
    #     steps = f'Vertically stretch by a factor of {abs(f.m)}.' if abs(f.m) != 1 else ''
    #     if f.m < 0:
    #         steps += ' Reflect over the x-axis.'
    #     if f.b != 0:
    #         steps += f' Tranlate {"up" if f.b > 0 else "down"} by {abs(f.b)} unit{"" if abs(f.b) == 1 else "s"}.'
    #     return (str(f), steps)

    # @staticmethod
    # def slope_point() -> ((float, (float, float)), str):
    #     """Practice finding a linear equation from its slope and a point on its line."""

    #     x1, y1 = randint(-5, 6, 2)
    #     m = Lin.happy_m()
    #     b = round(y1 - (m * x1), 2)
    #     b = int(b) if int(b) == b else b
    #     return ((m, (x1, y1)), Lin.from_str(m, b))

    # @staticmethod
    # def para_perp_point() -> ((str, str, (float, float)), str):
    #     """Practice finding linear equations from a point and a parallel or perpendicular function."""

    #     g = Lin((-5, 5), (-5, 5))
    #     numg, deng = Fraction(g.m).limit_denominator(5).as_integer_ratio()
    #     relation = choice(['para', 'perp'])
    #     num, den = (numg, deng) if relation == 'para' else (deng, numg)
    #     b = Lin.happy_m()
    #     x1 = randint(-3, 3) * den + b
    #     y1 = (num / den) * x1 + b

    #     return ((str(g), relation, (x1, y1)), Lin.from_str(num / den, b))
