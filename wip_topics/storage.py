"""Code Storage"""

"""STEP FUNCTIONS

Convert step function equations to graphs and back."""

from contextlib import contextmanager
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams['text.usetex'] = True
mpl.style.use('fivethirtyeight')
fig, ax = plt.subplots()

class Step:
    """y = m * |= x =| + b"""
    def __init__(self, m, b, round_down):
        # Does not support negative slopes yet.
        self.m, self.b, self.round_down = m, b, round_down

    def step_eq2graph(self):
        """Graph a step function.

        y = m * |= c * x =| + b
        Where |= , =| are the floor or ceiling function.

        Parameters:
            m: Slope, determines horizontal lines height.
            c: Inner coefficient, determines hline length. end - start = 1 / c
            b: Constant, determines y-intercept.
        """
        # DRAFT 1: m * floor(x) + b
        # TODO: Fix, doesn't work.

        x_vals = np.arange(-10, 10, 1/self.m)
        y = self.m * x_vals + (self.b if self.round_down else self.b + 1)
        print(y)

        with my_plot() as figax:
            fig, ax = figax
            starts = x_vals
            ends = x_vals + (1 / self.m)
            ax.hlines(y, starts, ends, colors='cornflowerblue', zorder=2)
            closed_markers, open_markers = (starts, ends) if self.round_down else (ends, starts)
            ax.scatter(closed_markers, y, c='k', linewidth='1', zorder=3)
            ax.scatter(open_markers, y, facecolors='w', edgecolors='k', linewidth='1', zorder=3)
            # Make closed and intervals.
            ax.set_title(r'$\left \lfloor{x}\right \rceil$')

@contextmanager
def my_plot(edge=10):
    fig, ax = plt.subplots()
    ax.set_xlim(-edge, edge)
    ax.set_xticks(np.arange(-edge, edge + 1))
    ax.set_ylim(-edge, edge)
    ax.set_yticks(np.arange(-edge, edge + 1))
    # Axis: short black with arrows? Semi-transparent lines?
    # ax.axhline(0, c='k', alpha=0.5)
    # ax.axvline(0, c='k', alpha=0.5)
    ax.annotate('', xy=(-edge, 0), xytext=(edge, 0), arrowprops=dict(arrowstyle='<|-|>', color='k'), size=20, zorder=1)
    ax.annotate('', xy=(0, -edge), xytext=(0, edge), arrowprops=dict(arrowstyle='<|-|>', color='k'), size=20, zorder=1)

    try:
        yield (fig, ax)

    finally:
        plt.show()

# # Figure out why it shows an empty figure.
# with my_plot() as figax:
#     fig, ax = figax
#     ax.plot(np.arange(-10, 11), np.random.randint(-10, 11, 21))

# Step(0.25, 0, True).step_eq2graph()
