"""Practice using z-scores and confidence intervals.

TODO:
Z-Score Drill
- Scale mean and stdev for true inf
- Better jargon for 'area of interest'
- Optimize where marked.
    - Simple way to randomly sample 2 different values?
CI Drill
- Should I round the margins of error or just the CI?
"""

from math import sqrt
from random import choice, gauss, randint, random, randrange
import numpy as np
from scipy.stats import norm

from drill_type import Drill

class Statistics:
    @staticmethod
    def zscore_drill():
        """For each normal distribution, find the probability that a value from that distribution is less than x.

        1. Make a normal distribution by picking the mean and stdev.
               Ex: X ~ N(mu = 10, sigma = 2)
        2. Choose whether the area of interest is to the left of a value 'P(X < x)',
           to the right of a value 'P(X > x)', or in between 2 values 'P(x1 < X < x2)'.
               Ex: Find P(X > x)
        3. If the area of interest is to the left or right of a value 'P(X < x) or P(X > x)':
               Pick a z-score and find the corresponding x value from the distribution.
               Ex: z = 1.6, x = sigma * z + mu = 13.2
           If the area of interest is in between 2 values 'P(x1 < X < x2)':
               Pick 2 z-scores and find the corresponding x values from the distribution.
        4. Find the probability that X is within the area of interest.
               Ex: z-table(1.6) = 0.9442
        """

        # TODO: For true inf: Scale mu and sigma, or widen interval for mu and sigma.
        #https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/effects-of-linear-transformations/v/how-parameters-change-as-data-is-shifted-and-scaled
        mu = randint(-100, 100)
        sigma = randrange(0, 51, 5)

        # 0: between, 1: less than, 2: greater than
        interesting_area = randint(0, 2)

        # If P(X < x) or P(X > x), pick x.
        if interesting_area:
            # Find a z-score and use it to calculate x.
            # This is because I want z to be within [-3.5, 3.5].
            z = randint(-350, 350) / 100
            # Use the z score formula to solve for x.
            x = round(sigma * z + mu, 2)
            # Use a z-table to find the area of the std norm dist that is to the left of z, or:
            # P(N(0, 1) < z)
            area_left = round(norm.cdf(z), 4)
            # P(X < x)
            if interesting_area == 1:
                # Ex: 1. X ~ N(mu = 93, sigma = 35). Find P(X < 87).
                question = f'X \\sim \\mathcal N (\\mu = {mu}, \\sigma = {sigma}) \\text{{. Find}} P(X < {x})\\text{{.}}'
                # question = f'X is a norm dist w/ \\mu = {mu} and \\sigma = {sigma} Find P(X < {x})'
                ans = str(area_left)

            # P(X > x)
            else:
                question = f'X \\sim \\mathcal N (\\mu = {mu}, \\sigma = {sigma}) \\text{{. Find }} P(X > {x})\\text{{.}}'
                # Find the area of the std norm dist that is to the right of z, or:
                # P(N(0, 1) > z) = 1 - P(N(0, 1) < z)
                area_right = round(1 - area_left, 4)
                ans = str(area_right)

        # If P(x1 < X < x2), pick x1 and x2.
        else:
            # TODO: Is there an easier way to make sure 2 vals aren't equal? {{{
            z1 = randint(-350, 350) / 100
            z2 = randint(-350, 350) / 100
            # Make sure they aren't equal
            while z2 == z1:
                z2 = randint(-350, 350) / 100
            # }}}

            z1, z2 = sorted([z1, z2])  # Make sure z1 < z2
            x1 = round(sigma * z1 + mu, 2)
            x2 = round(sigma * z2 + mu, 2)

            question = f'X \\sim \\mathcal N (\\mu = {mu}, \\sigma = {sigma}) \\text{{. Find}} P({x1} < X < {x2})\\text{{.}}'
            z1_area_left = norm.cdf(z1)
            z2_area_left = norm.cdf(z2)
            area_between = round(z2_area_left - z1_area_left, 4)
            ans = str(area_between)

        return Drill(question, ans)


    @staticmethod
    def conf_int_drill():
        """Calculate confidence intervals for the population parameters to 95\\% confidence.

        1. Choose sample size.
        2. Choose if pop para is mean or proportion.
        """

        n = randint(25, 100)

        coin_flip = np.random.random()
        if coin_flip < 0.5: # Sample proportion
            phat = round(random(), 2)
            M = 1 / sqrt(n)
            CI = (round(phat - M, 2), round(phat + M, 2))
            return Drill(f'\\hat{{p}} = {phat}, n = {n}', f'CI = ({CI[0]}, {CI[1]})')
        else: # Sample mean
            xbar = randint(-100, 100)
            s = randrange(0, 51, 5)
            M = 1.96 * (s / sqrt(n))
            CI = (round(xbar - M, 2), round(xbar + M, 2))
            return Drill(f'\\bar{{x}} = {xbar}, s = {s}, n = {n}', f'CI = ({CI[0]}, {CI[1]})')
