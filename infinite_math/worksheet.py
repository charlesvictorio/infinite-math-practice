#!/usr/bin/env python3

"""Make math worksheets with 1 function.

TODO:
Edit all drills to return Drill objects
Add header.
Add pictures.
Add tasks.
Allow for settings with kwargs
Option: seperate file for answer key

Worksheet(title)(drillers)

workshee.drills_to_make (list):
    Each element is (func, int).
    The first element of the tuple is a function that generates drills.
    It returns 2 strings, the 'problem' and the 'solution' (update after implementing figs).
    The second element of the tuple is an integer.
    It says how many drills to make for the drill function.

What this does:
1. Write preamble. This installs(?) packages, writes header, etc.
   The options are the same for every worksheet.
2. Generate drills (pairs of questions and their answers).
3. Render the lists of questions and answers to pdf using TeX.
   The docstrings of the drill functions are the instructions/prompts of the problems.
"""


from dataclasses import dataclass
import os
from typing import Callable

from inspect import cleandoc
from jinja2 import Environment, FileSystemLoader
from subprocess import run, DEVNULL

from drill_type import Drill
from polynomial import Polynomial
from quadratic import Quadratic
from statistics import Statistics
from differential_equations import DiffEqn
# from function_linear import Linear
from chemistry import AcidBaseEquilibrium

# Define types
# class Drill

@dataclass
class WorksheetSection:
    prompt: str
    drills: list[Drill]

# Settings = dict[str, str | int] # unsure of the best way to annotation the type of kwargs
DrillFunc = Callable[[], Drill]


class Worksheet:
    """Make a worksheet with generated problems rendered to latex."""

    def __init__(self, title='Worksheet'):
        """Create a worksheet object, to which drills can be added."""
        self.title = title #title.replace(' ', '_')
        self.drills_to_make: list[DrillFunc, int] = [] # "recipe" of which drills to make and how many of each

    def add_drill(self, drill: DrillFunc, qty: int):
        self.drills_to_make.append((drill, qty))

    def generate_all_drills(self) -> list[WorksheetSection]:
        """Generate the actual problems and solutions that will be shown on the worksheet."""
        worksheet_sections = []
        for drill_func, qty in self.drills_to_make:
            prompt = Worksheet.get_prompt(drill_func)
            drills = [drill_func() for _ in range(qty)]
            worksheet_sections.append(WorksheetSection(prompt, drills))
        return worksheet_sections

    def compile_worksheet(self) -> str:
        """Make a worksheet with the previously-added drills.
        
        Return the worksheet as a string in .tex file format.
        """
        worksheet_sections = self.generate_all_drills()
        environment = Environment(loader=FileSystemLoader('templates/'))
        template = environment.get_template("worksheet.tex")
        return template.render(title=self.title, sections=worksheet_sections)

    @staticmethod
    def get_prompt(func):
        """Get the prompt for problems from the docstring of its drill func."""
        return cleandoc(func.__doc__.split('\n\n', 1)[0])

    def write_file(self, output_type='tex', output_dir=None):
        if not output_dir:
            self.output_dir = os.path.join(os.getcwd(), 'out')
        self.pic_dir = os.path.join(self.output_dir, 'pics')
    
        os.makedirs('pics', exist_ok=True)

        worksheet_path_tex = os.path.join(self.output_dir, self.title.replace(' ', '_') + '.tex')

        tex_file_content = self.compile_worksheet()
        with open(worksheet_path_tex, 'w+') as f:
            f.write(tex_file_content)

        if output_type == 'tex':
            print(f'Successfully generated worksheet "{self.title}" at {self.worksheet_path}.tex')
        elif output_type == 'pdf':
            worksheet_path_pdf = os.path.join(self.output_dir, self.title + '.pdf')
            run(['pdflatex', '-halt-on-error', '-output-directory', self.output_dir, worksheet_path_pdf], stdout=DEVNULL, stderr=DEVNULL)
            print(f'Successfully generated worksheet "{self.title}" at {self.worksheet_path}.pdf')



if __name__ == '__main__': # maybe this goes in other file so drill can go here
    # drillers = [(Linear.dreb_drill, 2), (Linear.find_fx_drill, 2), (Linear.parent_transformations_drill, 4)]
    # drillers = [(Polynomial.poly_ops_drill, 2), (Quadratic.solve_quad_drill, 2), (Quadratic.convert2std_drill, 2), (Quadratic.cts_drill, 2)]

    w = Worksheet("Example Worksheet")

    # w.add_drill(Polynomial.foil_drill, 3)
    w.add_drill(Polynomial.poly_ops_drill, 2)
    w.add_drill(Quadratic.cts_drill, 2)
    w.add_drill(DiffEqn.constant_coefficients_drill, 3)
    # w.add_drill(Statistics.zscore_drill, 3)
    # w.add_drill(Statistics.conf_int_drill, 3)
    w.add_drill(AcidBaseEquilibrium.strong_acid_strong_base_equil_drill, 3)

    print(w.compile_worksheet())