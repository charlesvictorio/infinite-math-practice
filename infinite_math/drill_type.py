# no better place to put this i dont think

from dataclasses import dataclass
from typing import Optional

@dataclass
class Drill:
    problem: str
    solution: str
    # file name of picture associated with problem.
    # path of picture (if defined): `{worksheet.pic_dir}/{problemPic}`
    problemPic: Optional[str] = None
    solutionPic: Optional[str] = None