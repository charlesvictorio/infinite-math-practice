"""Apply operations (+,-,*) on polynomials."""

__all__ = ['Polynomial']

from drill_type import Drill

from functools import reduce
import operator
import numpy as np
# from numpy.random import choice, randint
from sympy import *
x, y, z = symbols('x y z')

MAX_COEFF = 10

class Polynomial:
    @staticmethod
    def gen(degree=1):
        """Generate a single-variable polynomial of a specified degree."""
        
        coeffs = [] # stored in reverse order. each term: `coeff[i] * x ^ i`
        for _ in range(degree): # coeffs of lower terms
            coeffs.append(np.random.randint(-MAX_COEFF, MAX_COEFF + 1))
        coeffs.append(np.random.randint(1, MAX_COEFF + 1)) # guarantee the leading coeff is non-zero

        polyn = 0
        for i in range(degree + 1):
            polyn += coeffs[i] * x ** i
        return polyn
    
    def gen_multivar_term(max_degree=2):
        """Generate a term of a multi-variable polynomial."""

        coeff = np.random.randint(1, MAX_COEFF + 1)
        coeff *= np.random.choice([-1, 1])
        max_amount_of_exponents = 3
        amount_of_exponents = np.random.randint(1, max_amount_of_exponents + 1)

        term = coeff
        for _ in range(amount_of_exponents):
            variable = np.random.choice([x, y, z]) # intentionally allow repeats
            exponent = np.random.randint(1, max_degree + 1)
            term *= variable ** exponent
        return term


    @staticmethod
    def poly_ops_drill():
        """Simplify these polynomials.

        Question: 2 polynomials added/subtracted/multiplied together.
        Answer: 1 simplified polynomial: The sum/difference/product of the 2 in question.
        """

        max_degree = 3
        p1 = Polynomial.gen(np.random.randint(1, max_degree + 1))
        p2 = Polynomial.gen(np.random.randint(1, max_degree + 1))
        op_str = np.random.choice(['+', '-', '*'])
        problem = f'({latex(p1)}) {op_str} ({latex(p2)})'

        op_func = {'+': operator.add, '-': operator.sub, '*': operator.mul}
        compute_operation = op_func[op_str](p1, p2) # Actually perform the operation to get the simplified polynomial
        solution = latex(expand(compute_operation))
        return Drill(problem, solution)

    @staticmethod
    def foil_drill():
        """Expand these factored polynomials using the FOIL method."""
        a, b, c, d = [Polynomial.gen_multivar_term() for _ in range(4)]
        factored_form = (a + b) * (c + d)
        expanded_form = expand(factored_form)
        return Drill(latex(factored_form), latex(expanded_form))

    @staticmethod
    def pst_dots_drill():
        """Simplify these special patterns of polynomials.

        Perfect square trinomials: (a + b)^2 = a^2 + 2ab + b^2
        Difference of two squares: (a + b)(a - b) = a^2 - b^2
        """

        a = Polynomial.gen_multivar_term()
        b = Polynomial.gen_multivar_term()
        if np.random.random() < 0.5: # Perfect square trinomial
            factored_form = (a + b) ** 2
            expanded_form = expand(factored_form)
        else: # Difference of two squares
            factored_form = (a + b) * (a - b)
            expanded_form = expand(factored_form)
        # return Drill(latex(factored_form), latex(expanded_form))
        return Drill(latex(expanded_form), latex(factored_form))

    @staticmethod
    def division_drill():
        """Divide these polynomials."""
        # TODO: Add remainders
        divisor = Polynomial.gen(np.random.randint(1, 3))
        quotient = Polynomial.gen(np.random.randint(1, 3))
        dividend = expand(divisor * quotient)
        return Drill(latex(dividend / divisor), latex(quotient))