from math import log, floor, log10

import numpy as np

from drill_type import Drill

class AcidBaseEquilibrium:
    nice_strong_acids = ["HCl", "HBr", "HI", "HClO_4", "HNO_3"]
    # strong_bases = ["NaOH", "KOH", "LiOH", "Sr(OH)_2", "Ba(OH)_2", "Ca(OH)_2"]
    nice_strong_bases = ["NaOH", "KOH", "LiOH"]
    round_to_n = lambda x, n: x if x == 0 else round(x, -int(floor(log10(abs(x)))) + (n - 1)) # https://stackoverflow.com/questions/3410976/how-to-round-a-number-to-significant-figures-in-python
    def strong_acid_strong_base_equil_drill():
        """Calculate the pH of these solutions.
        
        Assume standard conditions.
        """
        acid = np.random.choice(AcidBaseEquilibrium.nice_strong_acids)
        base = np.random.choice(AcidBaseEquilibrium.nice_strong_bases)
        acid_molarity = AcidBaseEquilibrium.round_to_n(np.random.random() * 10 ** np.random.randint(-3, 2), 3)
        base_molarity = AcidBaseEquilibrium.round_to_n(np.random.random() * 10 ** np.random.randint(-3, 2), 3)
        acid_volume = np.random.randint(1, 100) / 1000 # mL
        base_volume = np.random.randint(1, 100) / 1000 # mL
        problem = f"{acid_volume} \\text{{ L of }} {acid_molarity} \\text{{ M }} \\ce{{{acid}}} \\text{{ mixed with }} {base_volume} \\text{{ L of }} {base_molarity} \\text{{ M }} \\ce{{{base}}}"

        acid_moles = acid_volume * acid_molarity
        base_moles = base_volume * base_molarity
        excess_reactant_moles = abs(acid_moles - base_moles)
        excess_reactant_molarity = excess_reactant_moles / (acid_volume + base_volume)
        p_excess_reactant = -log(excess_reactant_molarity, 10)
        if acid_moles > base_moles:
            pH = p_excess_reactant
        else:
            pH = 14 - p_excess_reactant
        return Drill(problem, f'pH = {pH:.2f}')