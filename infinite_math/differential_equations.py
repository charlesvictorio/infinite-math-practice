from drill_type import Drill
from quadratic import Quadratic

import numpy as np
from sympy import *

t = symbols('t')
y = Function('y')(t)
yp = y.diff(t)
ypp = y.diff(t, t)

c1, c2 = symbols ('c_1, c_2')

class DiffEqn:
    def latex_prime(expr):
        """Return the latex format of a DE, but with y' instead of d/dx."""
        # needs to be removed in this order
        return latex(expr).replace(r'\frac{d^{2}}{d t^{2}} y{\left(t \right)}', "y''").replace(r'\frac{d}{d t} y{\left(t \right)}', "y'").replace(r'y{\left(t \right)}', r'y')

    def constant_coefficients_drill():
        r"""Solve these DE's with the method of constant coefficients."""
        
        # Second order linear homogenous DE's with all constant coefficients.
        # a y'' + b y' + c = 0
        # Factor characteristic equation: ar^2 + br + c = 0
        # 2 real roots (r = r_1, r = r_2): y_c = c_1 e^{r_1 t} + c_2 e^{r_2 t}
        # 1 real root (r = r_1): y_c = c_1 e^{r_1 t} + c_2 t e^{r_1 t}
        # 2 complex roots (r = \alpha \pm \beta i): y_c = e^{\alpha t} ( c_1 * \cos \beta t + c_2 \sin \beta t)
        
        # Maybe skip using sympy and just format by hand
        
        coin_flip = np.random.random()
        if coin_flip < 1 / 3: # 2 real roots
            a, b, c, r1, r2 = Quadratic.gen_2_real_roots()
            solution_expr = c1 * E ** (r1 * t) + c2 * E ** (r2 * t)
        elif coin_flip < 2 / 3: # 1 real root
            a, b, c, r1 = Quadratic.gen_1_real_root()
            solution_expr = c1 * E ** (r1 * t) + c2 * t * E ** (r1 * t)
        else: # 2 complex roots
            a, b, c, alpha, beta = Quadratic.gen_2_complex_roots()
            solution_expr = E ** (alpha * t) * (c1 * cos(beta * t) + c2 * sin(beta * t))
        
        problem_expr = a * ypp + b * yp + c * y
        problem = DiffEqn.latex_prime(problem_expr) + ' = 0'
        solution = "y = " + latex(solution_expr)
        return Drill(problem, solution)