"""Quadratic drills

Convert quadratic equations between standard, vertex, factored forms (involves irrational and complex numbers).
Find solutions to quadratic expressions. Use inverse operations, zero product property, completing the square, quadratic formula.
"""

__all__ = ['Quadratic']

from drill_type import Drill

import numpy as np
from numpy.random import choice, randint, random
from sympy import *
x = symbols('x')


class Quadratic:
    @staticmethod
    def gen(a_range=(1, 4), b_range=(-10, 11), c_range=(-10, 11)):
        """Generate a quadratic expression with small integers coefficients in standard form."""
        a = randint(*a_range) * choice([1, -1]) # Can't do randint(-3, 4) because leading term shouldn't be 0.
        b = randint(*b_range)
        c = randint(*c_range)
        return a * x ** 2 + b * x + c

    @staticmethod
    def gen_vert():
        """Generate a quadratic expression with small integer coefficients in vertex form."""
        a = randint(1, 4) * choice([1, -1])
        h = randint(1, 11) * choice([1, -1])
        k = randint(-10, 11)
        return a * (x - h) ** 2 + k
    
    @staticmethod
    def gen_2_real_roots():
        """Generate a quadratic expression which has two real roots.
        
        y = a(x - r_1)(x - r_2)
        y = a x^2 + (-a(r_1 + r_2)) x + a r_1 r_2
        Returns the equation's coefficients and the two roots.
        """
        max_root = 10
        r1 = np.random.randint(-max_root, max_root + 1)
        r2 = np.random.randint(-max_root, max_root + 1)
        while r2 == r1: # ensure two distinct roots
            r2 = np.random.randint(-max_root, max_root + 1)
        a = np.random.randint(1, 4) * np.random.choice([1, -1]) # ensure small non-zero a
        b = -a * (r1 + r2)
        c = a * r1 * r2
        return (a, b, c, r1, r2)

    @staticmethod
    def gen_1_real_root():
        """Generate a quadratic expression which has one real root.
        
        y = a(x - r_1)^2
        y = a x^2 + (-2 a r_1) x + a * r_1^2
        Returns the equation's coefficients and the one root.
        """
        max_root = 10
        r1 = np.random.randint(-max_root, max_root + 1)
        a = np.random.randint(1, 4) * np.random.choice([1, -1])
        b = -2 * a * r1
        c = a * r1 * r1
        return (a, b, c, r1)
    
    @staticmethod
    def gen_2_complex_roots():
        r"""Generate a quadratic expression which has two complex roots.
        
        y = a(x - \alpha - \beta * i)(x - \alpha + \beta * i)
        y = a x^2 + (-2 * \alpha * a) x + a (\alpha ^ 2 + \beta ^ 2)
        Returns the equation's coefficients and \alpha and \beta, where two roots are x = \alpha \pm \beta * i
        """
        max_num = 10
        alpha = np.random.randint(-max_num, max_num + 1)
        beta = np.random.randint(1, max_num + 1) * np.random.choice([1, -1]) # ensure nonzero beta
        a = np.random.randint(1, 4) * np.random.choice([1, -1]) # ensure nonzero a
        b = -2 * alpha * a
        c = a * (alpha * alpha + beta * beta)
        return (a, b, c, alpha, beta)


    @staticmethod
    def std_fac_drill(std2fac=True):
        """Practice converting quadratic equations between standard and factored form."""

        a = randint(1, 4) * choice([1, -1])
        # To have 1 irrational solution, b must be irrational.
        # I want all coefficients to be rational, so there is no '1 irrat sol' option.
        sol_type = choice(['2 rat sols', '1 rat sol', '2 irrat sols', '2 complex sols'])

        if sol_type == '2 rat sols':
            # Choose 2 different factors.
            r1, r2 = choice(np.arange(-3, 4), 2, replace=False)
            # This cleans the generated `x * (1 - x)` to the nicer-looking `-x * (x - 1)`.
            std_form = expand(a * (x - r1) * (x - r2))
            fac_form = factor(std_form)
        elif sol_type == '1 rat sol':
            r = randint(-3, 4)
            fac_form = a * (x - r) ** 2
            std_form = expand(fac_form)
        elif sol_type == '2 irrat sols':
            rat_const = randint(-10, 11)
            irrat_coeff = randint(1, 11) * choice([1, -1])
            radicand = choice([2, 3, 5, 6, 7, 10])  # Add more non-factorable radicands (11, 13, 15, ...) if I need more possibilities.
            r1 = rat_const + irrat_coeff * sqrt(radicand)
            r2 = rat_const - irrat_coeff * sqrt(radicand)
            # Factor out the coefficient: (x - 1)*(2*x + 6) => 2*(x - 1)*(x + 3)
            fac_form = factor(a * (x - r1) * (x - r2))
            std_form = expand(fac_form)
        else:  # 2 complex solutions
            # Roots are complex conjugates.
            # Complex numbers: z = a + bi
            a_ = randint(-10, 11)
            b = randint(1, 11) * choice([1, -1])
            r1, r2 = a_ + b*I, a_ - b*I
            # Factor out the coefficient: (x - 1)*(2*x + 6) => 2*(x - 1)*(x + 3)
            fac_form = factor(a * (x - r1) * (x - r2))
            std_form = expand(fac_form)
        a, b = (std_form, fac_form) if std2fac else (fac_form, std_form)
        return Drill(latex(a), latex(b))

    @staticmethod
    def vert_std_drill(vert2std=True):
        """Convert quadratic equations between vertex and standard form."""

        vert_form = Quadratic.gen_vert()
        std_form = expand(vert_form)

        a, b = (vert_form, std_form) if vert2std else (std_form, vert_form)
        return Drill(latex(a), latex(b))

    @staticmethod
    def fac_vert_drill(fac2vert=True):
        """Convert quadratic equations between factored and vertex form."""

        vert_form = Quadratic.gen_vert()
        std_form = expand(vert_form)

        a = LC(poly(std_form))
        facs = list(solveset(std_form))
        if len(facs) == 1:
            fac_form = factor(a * (x - facs[0]) ** 2)
        else:
            # Return `-3 * (x - 2 + sqrt(3)/3) * (x - 2 - sqrt(3)/3)` instead of `-(3*x - 6 - sqrt(3))*(3*x - 6 + sqrt(3))/3`.
            # That error only affected equations with fractions as roots I think.
            fac_form = Mul(a, x - facs[0], x - facs[1], evaluate=False)

        a, b = (fac_form, vert_form) if fac2vert else (vert_form, fac_form)
        return Drill(latex(a), latex(b))

    @staticmethod
    def convert2std_drill():
        """Convert these quadratic expressions to standard form."""

        return Quadratic.std_fac_drill(False) if random() < 0.5 else Quadratic.vert_std_drill(True)

    @staticmethod
    def convert2fac_drill():
        """Convert these quadratic expressions to factored form."""

        return Quadratic.std_fac_drill(True) if random() < 0.5 else Quadratic.fac_vert_drill(False)

    @staticmethod
    def convert2vert_drill():
        """Convert these quadratic expressions to vertex form."""

        return Quadratic.vert_std_drill(False) if random() < 0.5 else Quadratic.fac_vert_drill(True)

    @staticmethod
    def inv_ops_drill():
        """Solve these quadratic expressions using inverse operations.

        To use inverse operations, b != 0.
        ax^2 + c = 0 => ax^2 = -c => x^2 = -c/a => x = +- sqrt(-c/a)
        """

        a = randint(1, 4) * choice([1, -1])
        c = randint(-100, 101)
        eq = a * x ** 2 + c
        pos_x = sqrt(Rational(-c, a))
        return Drill(latex(eq), f'x = {latex(pos_x)}, {latex(-pos_x)}')

    @staticmethod
    def zpp_drill():
        """Solve these quadratic expressions using zero product property.

        To use zpp, solutions must be rational.
        Choose a, choose sols that are rational.
        """

        a = randint(1, 4) * choice([1, -1])
        coeff1 = randint(1, 11) * choice([1, -1])
        coeff2 = randint(1, 11) * choice([1, -1])
        const1, const2 = randint(-10, 11, size=2)

        fac_form = factor((coeff1 * x + const1) * (coeff2 * x + const2))
        std_form = expand(fac_form)
        # No vertex form because then inv ops would be the better method.
        eq = choice([fac_form, std_form])
        sols = [latex(sol) for sol in solveset(std_form)]
        sol_str = 'x = ' + ', '.join(sols)
        return Drill(latex(eq), sol_str)

    @staticmethod
    def cts_drill():
        """Solve these quadratic expressions using completing the square.

        To use cts, b/a must be even.
        """

        a = randint(1, 4) * choice([1, -1])
        b = 2 * a * randint(1, 6) * choice([1, -1])
        c = randint(1, 11) * choice([1, -1])
        std_form = a * x ** 2 + b * x + c
        sols = [latex(sol) for sol in solveset(std_form)]
        sol_str = 'x = ' + ', '.join(sols)
        return Drill(latex(std_form), sol_str)

    @staticmethod
    def quad_form_drill():
        """Solve these quadratic expressions using the quadratic formula.

        To force students to use the formula instead of any other strategy,
        the solutions must be irrational and/or complex.
        """

        a = randint(1, 4) * choice([1, -1])
        # Choose if solutions have irrational and/or imaginary parts.
        real_part = randint(1, 11) * choice([1, -1])
        unfactorable = choice(['irrat', 'imag', 'both'])
        ip = ((randint(1, 11) if unfactorable == 'imag' else sqrt(choice([2, 3, 5, 6, 7, 10])))
             * (1 if unfactorable == 'irrat' else I))
        r1 = real_part + ip
        r2 = real_part - ip
        std_form = expand(a * (x - r1) * (x - r2))
        sol_str = f'x = {latex(r1)}, {latex(r2)}'
        return Drill(std_form, sol_str)

    def solve_quad_drill():
        """Solve these quadratic expressions by any means necessary."""

        std_form = Quadratic.gen()
        sol_str = 'x = ' + ', '.join([latex(sol) for sol in solveset(std_form)])
        return Drill(latex(std_form), sol_str)
