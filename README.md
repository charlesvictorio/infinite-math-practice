# Infinite ~~Math~~ Practice

Generate infinite worksheets.

Although this started out only doing high-school math, I am using it for any subject I want to learn more about.
This helps practice computation, but is not a substitute for conceptually understanding the problem.

# How It Works

To make a drill, I write an algorithm to go between the problem and the solution.
Writing these algorithms and ensuring their correctness helps me solidify my understanding of the subject.
Each time a worksheet is generated, I randomly generate numbers for each problem and calculate what the solutions should be based on those numbers (or work backwards).
I have to choose numbers that are the right magnitude and significance for the problem.

The generated and calculated numbers can be plugged into a word problem.
This problem is then stored in a Drill dataclass (an algebraic data type, basically a struct).
To make an entire worksheet, first the user needs to specify what drills to include in it, and how many of them.
Then, the program generates unique problems and plugs them into a .tex template of a worksheet.
This file can then be converted into a pdf.

# Current Drills

## Quadratics

- Convert between standard, factored, and vertex form.
- Find the root(s) with inverse operations, the zero product property, completing the square, and the quadratic formula.

## Polynomials

- Addition, subtraction, multiplication, and division on two polynomials.
- Expand factors with the FOIL method.
- Factor polynomials fitting the perfect square trinomial or difference of two square patterns

## Statistics

- Use z-scores to find probability.
- Calculate confidence intervals.

## Differential Equations

- Method of constant coefficients.

## Chemistry
- pH of strong-acid strong-base solution.

# TODO

- Make a website that generates drills one at a time.
  - The base library will contain drill-generating functions, but it can be viewed online or as a pdf. 
- Add option to show step-by-step solutions in the answer key.
- Add support for pictures, including matplotlib graphs.
// - Use TeX task package.
- Formally prove the correctness of the algorithms if possible.