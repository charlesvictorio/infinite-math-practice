# Info about how I made this, etc

Each function generates (question, answers).
Ex: ('x**2 - 1', 'x = -1, 1')

The prompt is the function's docstring.
Ex: """Find the solutions to these expressions."""

don't execute with 'python3.8 -OO', since this relies on docstrings.

Drill ADT should contain math markup $$ when applicable, not in template